import React, { Component } from 'react';
import { number } from 'prop-types';

class App extends Component<{}, IState> {

  constructor(props: {}) {
    super(props);
    this.state = {
      currentTask: "",
      tasks: []
    }
  }

  public handleSubmit(e: React.FormEvent<HTMLFormElement>): void {
    e.preventDefault();
    this.setState({
      currentTask: "",
      tasks: [
        ...this.state.tasks,
        {
          id: this._timeInMilliseconds(),
          value: this.state.currentTask,
          completed: false
        }
        
      ]
    })
  }

  public renderTasks(): JSX.Element[] {
    return this.state.tasks.map((task: ITask, i: number) => {
      return (
        <div key={task.id}>
          <span>{task.value}</span>
          <button onClick={ () => this.deleteTask(task.id)}>Delete</button>
          <button onClick={ () => this.toggleDone(i) }>Done</button>
        </div>
      )
    })
  }

  public deleteTask(id: number): void {
    const filteredTasks: Array<ITask> = this.state.tasks.filter((task: ITask) => task.id !== id);
    this.setState({
      tasks: filteredTasks
    })
  }

  public toggleDone(i: number): void {
    let task: ITask[] = this.state.tasks.splice(i, 1);
    task[0].completed = !task[0].completed;
    const currentTasks: ITask[] = [...this.state.tasks, ...task]
    this.setState({ tasks: currentTasks });
  }


  public render(): JSX.Element { 
    return (
      <div className="App">
        <h1>React Typescript ToDo List</h1>
        <form onSubmit={ (e) => this.handleSubmit(e) }>
          <input 
            type="text" 
            onChange={ (e) => this.setState({currentTask: e.target.value}) } 
            value={this.state.currentTask} />
          <button type="submit">Add Task</button>
        </form>
        <section>{this.renderTasks()}</section>
      </div>
    );
  }

  private _timeInMilliseconds(): number {
    const date: Date = new Date();
    return date.getTime();
  }


}

interface IState {
  currentTask: string;
  tasks: Array<ITask>;
}

interface ITask {
  id: number;
  value: string;
  completed: boolean;
}

export default App;
